## RESTful Pets Store

Reference: *where did this Project come from?*   TBD...

### Notes

 * very well-structured Flask-RESTful based Project
 
 * uses *Flask apispec* -- https://pypi.org/project/flask-apispec/
 
 * uses *Flask-Marshmallow* for (de)serialization

 * uses *Flask-JWT* for authentication

 * gets ``FLASK_ENV`` via the Py ``dotenv`` module

 * builds a Docker-app using a ``Dockerfile``, and ``docker-compose.yml``

Et cetera -- more details coming


### Project Setup

